package com.nimvb.app.spacestation.collector.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;

import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
public class Client {

    private final WebSocketClient client;
    private final String URL;

    public CompletableFuture<WebSocketSession> connect(WebSocketHandler handler){
        return client.doHandshake(handler,URL).completable();
    }
}
