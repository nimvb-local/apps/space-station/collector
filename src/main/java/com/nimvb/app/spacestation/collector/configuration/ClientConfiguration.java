package com.nimvb.app.spacestation.collector.configuration;

import com.nimvb.app.spacestation.collector.service.Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.client.WebSocketClient;

@Configuration
public class ClientConfiguration {

    @Bean
    Client client(WebSocketClient client, @Value("${websocket.server.url}") String url){
        return new Client(client,url);
    }
}
