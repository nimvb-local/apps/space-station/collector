package com.nimvb.app.spacestation.collector;

import com.nimvb.app.spacestation.collector.service.Client;
import com.nimvb.app.spacestation.collector.service.Handler;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;

@SpringBootApplication
@RequiredArgsConstructor
@EnableBinding(Source.class)
public class CollectorApplication implements CommandLineRunner {

    private final Client client;
    private final Handler handler;

    public static void main(String[] args) {
        SpringApplication.run(CollectorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        client.connect(handler).thenRun(() -> {
            System.out.println("connected!");
        });
    }
}
