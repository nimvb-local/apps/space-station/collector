package com.nimvb.app.spacestation.collector.service;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Publisher {
    private final Source source;



    public void send(String message){
        source.output().send(MessageBuilder.withPayload(message).build());
    }
}
